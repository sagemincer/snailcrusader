# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from textpush import pipe

def hash_skip_hashbang(target):
    while True:
        value = (yield)
        if value[0] == 'new_file':
            first_line = True
        elif value[0] == 'line':
            line, lineno = value[1:]
            if first_line:
                if line.startswith('#!'):
                    # ignore
                    continue
            if line.startswith('#'):
                target.send( ('comment', line[1:], lineno) )
            else:
                target.send( ('toplevel', line, lineno) )
        else:
            pass

def hash_or_semicolon(target):
    while True:
        value = (yield)
        if value[0] == 'line':
            line, lineno = value[1:]
            if line.startswith(';'):
                target.send( ('comment', line[1:], lineno) )
            elif line.startswith('#'):
                target.send( ('comment', line[1:], lineno) )
            else:
                target.send( ('toplevel', line, lineno) )
        else:
            pass


def percent(target):
    while True:
        value = (yield)
        if value[0] == 'line':
            line, lineno = value[1:]
            if line.startswith('%'):
                target.send( ('comment', line[1:], lineno) )
            else:
                target.send( ('toplevel', line, lineno) )
        else:
            pass
    
def erb_tag_contents(target):
    in_tag = False
    while True:
        value = (yield)
        if value[0] == 'line':
            line, lineno = value[1:]
            if in_tag:
                try:
                    before_first_end_tag = line[:line.index('%>')]
                    if '<%' not in before_first_end_tag:
                        target.send( ('in_tag', before_first_end_tag, lineno) )
                        in_tag = False
                except ValueError:
                    # the index failed, no %> in line
                    target.send( ('in_tag', line, lineno) )
                # ending tags, then complete tags
                complete_tags = re.findall(r'<%([^%]+(%[^>]+)*)%>', line)
                for tag_contents, _ in complete_tags:
                    target.send( ('in_tag', tag_contents, lineno) )
            else:
                # complete tags, then tags that start but don't finish
                complete_tags = re.findall(r'<%([^%]+(%[^>]+)*)%>', line)
                for tag_contents, _ in complete_tags:
                    target.send( ('in_tag', tag_contents, lineno) )
                try:
                    after_last_begin_tag = line[line.rindex('<%')+2:]
                    if '%>' not in after_last_begin_tag:
                        target.send( ('in_tag', after_last_begin_tag, lineno) )
                        in_tag = True
                except ValueError:
                    # the rindex failed, no <% in line
                    pass
        else:
            pass

def comments_in_erb_tag_contents(target):
    while True:
        value = (yield)
        if value[0] == 'in_tag':
            contents, lineno = value[1:]
            if len(contents) == 0:
                pass
            elif contents[0] == '=':
                # this is a <%=varname%> sort of construct, no room
                # for comments
                pass
            else:
                salient = contents.strip().strip('-').strip()
                if salient.startswith('#'):
                    target.send( ('comment', salient[1:], lineno) )
        else:
            pass

def erb_comments(target):
    return pipe(erb_tag_contents, comments_in_erb_tag_contents, target)
