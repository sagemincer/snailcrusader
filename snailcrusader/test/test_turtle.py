# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from textpush.test import CoroutineTest
from snailcrusader.turtle import (
    emit_turtle_prelude, emit_turtle_comments)
import unittest

class TestEmitTurtlePrelude(CoroutineTest):
    coroutine_under_test = emit_turtle_prelude('http://example.org/')
    
    send = [
        ('new_file', 'manifests/init.pp'),
        ('line', '#!/hash/bang'),
        ('line', '#t comment'),
        ('line', 'class foo {}'),
    ]

    expect = [
        ('turtle_prelude', '@base <http://example.org/manifests/init.pp> .'),
    ]


class TestEmitTurtleComments(CoroutineTest):
    coroutine_under_test = emit_turtle_comments
    
    send = [
        ('new_file', '/home/fantasy/manifests/init.pp',
         'manifests/init.pp'),
        ('line', '#t <> a sageprov:Flarble'),
        ('comment', 't <> a sageprov:Flarble'),
        ('line', 'class foo {}'),
    ]

    expect = [
        ('turtle_content', '<> a sageprov:Flarble'),
    ]

    
if __name__ == '__main__':
    unittest.main()
