# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from textpush.test import CoroutineTest
from snailcrusader.commentfinders import (
    hash_skip_hashbang, hash_or_semicolon, erb_tag_contents,
    erb_comments, percent)
import unittest

class TestHashSkipHashbang(CoroutineTest):
    coroutine_under_test = hash_skip_hashbang
    
    send = [
        ('new_file', 'manifests/init.pp'),
        ('line', '#!/hash/bang', 1),
        ('line', '# comment', 2),
        ('line', 'class foo {}', 3),
    ]

    expect = [
        ('comment', ' comment', 2),
        ('toplevel', 'class foo {}', 3),
    ]

class TestHashOrSemicolon(CoroutineTest):
    coroutine_under_test = hash_or_semicolon
    
    send = [
        ('new_file', 'files/paster.ini'),
        ('line', '[foo]', 1),
        ('line', '# comment', 2),
        ('line', '; another comment', 3),
        ('line', 'key = value', 4),
    ]

    expect = [
        ('toplevel', '[foo]', 1),
        ('comment', ' comment', 2),
        ('comment', ' another comment', 3),
        ('toplevel', 'key = value', 4),
    ]

class TestPercent(CoroutineTest):
    coroutine_under_test = percent
    
    send = [
        ('new_file', 'files/bla.tex'),
        ('line', r'\begin{document}', 1),
        ('line', r'% comment', 2),
        ('line', r'Lorem ipsum dolor sit amet', 3),
    ]

    expect = [
        ('toplevel', r'\begin{document}', 1),
        ('comment', ' comment', 2),
        ('toplevel', 'Lorem ipsum dolor sit amet', 3),
    ]


class TestErbContents(CoroutineTest):
    coroutine_under_test = erb_tag_contents

    send = [
        ('new_file', 'templates/fun.erb'),
        ('line', '<%=foo-%> <%-bar -%><% baz %><% bletch', 1),
        ('line', ' # foo bar baz %><%=wefoijwfeoi%>', 2),
        ('line', 'toplevel1', 3),
        ('line', '<%', 4),
        ('line', ' # foo % bar > % > baz %>fnord<%=wefoijwfeoi%>', 5),
    ]

    expect = [
        ('in_tag', '=foo-', 1),
        ('in_tag', '-bar -', 1),
        ('in_tag', ' baz ', 1),
        ('in_tag', ' bletch', 1),
        ('in_tag', ' # foo bar baz ', 2),
        ('in_tag', '=wefoijwfeoi', 2),
        ('in_tag', '', 4),
        ('in_tag', ' # foo % bar > % > baz ', 5),
        ('in_tag', '=wefoijwfeoi', 5),
    ]


class TestErbComments(CoroutineTest):
    coroutine_under_test = erb_comments

    send = [
        ('new_file', 'templates/fun.erb'),
        ('line', '<%=foo-%> <%-#bar -%><% # baz %><% # bletch', 1),
        ('line', ' # foo bar baz %><%=wefoijwfeoi%>', 2),
        ('line', '# toplevel1', 3),
        ('line', '<%', 4),
        ('line', ' # foo % bar > % > baz %>fnord<%=wefoijwfeoi%>', 5),
    ]

    expect = [
        ('comment', 'bar', 1),
        ('comment', ' baz', 1),
        ('comment', ' bletch', 1),
        ('comment', ' foo bar baz', 2),
        ('comment', ' foo % bar > % > baz', 5),
    ]

class TestErbCommentsHardRe1(CoroutineTest):
    coroutine_under_test = erb_comments

    send = [
        ('new_file', 'templates/fun.erb'),
        ('line', 'bb -b <%= bbbbbbbbbbb_bbbbbb_bbb -%> \\', 1),
        ]

    expect = [
        ]


class TestErbCommentsHardRe2(CoroutineTest):
    coroutine_under_test = erb_comments

    send = [
        ('new_file', 'templates/fun.erb'),
        ('line', 'AAAAA_AAA=<%= bbbbb_bbbbbbbbb %>', 1),
        ('line', "# AAA, bbbbbb bbbbbb bbbbb bb b bbbbbbbb bbbb bbb bbb bbb bbb bbbb Abbbb", 2),
        ]

    expect = [
        ]


if __name__ == '__main__':
    unittest.main()
