# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from textpush import prime
from textpush.test import CoroutineTest
from contextlib import contextmanager
from ..inputs import (
    lines_through_commentfinder, interesting_files_on_disk,
    interesting_files_in_zip)
import unittest
from shutil import rmtree
from tempfile import mkdtemp
import os
from os.path import join as pjoin
import re
from zipfile import ZipFile

def contents_to_lines(text):
    for x in text.split('\n'):
        yield x + '\n'

def contents_to_context(text):
    @contextmanager
    def context():
        yield contents_to_lines(text)
    return context

foo_pp = '''\
#!hashbang
# comment1
#t comment2
code {
}
#t comment3
'''

bar_ini = '''\
[section]
;t comment1
; comment2
# comment3
option = value
'''

class TestLinesThroughCommentfinder_pp(CoroutineTest):
    maxDiff = None
    coroutine_under_test = lines_through_commentfinder()
    send = [
        ('interesting', 'hash_skip_hashbang',
         'manifests/foo.pp', contents_to_context(foo_pp)),
    ]

    expect = [
        ('new_file', 'manifests/foo.pp'),
        ('line', '#!hashbang', 1),
        ('line', '# comment1', 2),
        ('comment', ' comment1', 2),
        ('line', '#t comment2', 3),
        ('comment', 't comment2', 3),
        ('line', 'code {', 4),
        ('toplevel', 'code {', 4),
        ('line', '}', 5),
        ('toplevel', '}', 5),
        ('line', '#t comment3', 6),
        ('comment', 't comment3', 6),
        ('line', '', 7),
        ('toplevel', '', 7),
        ('end_of_file', 'manifests/foo.pp'),
    ]


class TestLinesThroughCommentfinder_ini(CoroutineTest):
    maxDiff = None
    coroutine_under_test = lines_through_commentfinder()
    send = [
        ('interesting', 'hash_or_semicolon',
         'bar.ini', contents_to_context(bar_ini)),
    ]

    expect = [
        ('new_file', 'bar.ini'),
        ('line', '[section]', 1),
        ('toplevel', '[section]', 1),
        ('line', ';t comment1', 2),
        ('comment', 't comment1', 2),
        ('line', '; comment2', 3),
        ('comment', ' comment2', 3),
        ('line', '# comment3', 4),
        ('comment', ' comment3', 4),
        ('line', 'option = value', 5),
        ('toplevel', 'option = value', 5),
        ('line', '', 6),
        ('toplevel', '', 6),
        ('end_of_file', 'bar.ini'),
    ]


class HasTempDir(unittest.TestCase):
    def setUp(self):
        self.dir = mkdtemp('test_inputs')
    def tearDown(self):
        rmtree(self.dir)


class TestInterestingFilesOnDisk(HasTempDir):
    def setUp(self):
        super(TestInterestingFilesOnDisk, self).setUp()
        os.mkdir(pjoin(self.dir, '.git'))
        os.mkdir(pjoin(self.dir, 'test'))
        os.mkdir(pjoin(self.dir, 'cool'))
        with open(pjoin(self.dir, 'cool', 'file.ini'), 'w') as f:
            f.write('''\
#t <> <> <> .
[main]
foo = bar
''')
        with open(pjoin(self.dir, '.git', 'file.ini'), 'w') as f:
            f.write('this should be skipped')
        with open(pjoin(self.dir, 'test', 'file.ini'), 'w') as f:
            f.write('this should be skipped')
    def testInterestingFiles(self):
        decision_list = [(re.compile(r'.*\.ini$'), 'ini file')]
        prune_dirs = ['.git', 'test']
        results = []
        def adder():
            while True:
                results.append((yield))
        target = prime(adder())
        interesting_files_on_disk(self.dir, decision_list, 
                                  prune_dirs, target)
        # the last thing is going to be a lambda, and so will be
        # unique every time
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0][:3],
                         ('interesting', 'ini file', 'cool/file.ini'))


class TestInterestingFilesInZip(HasTempDir):
    def setUp(self):
        super(TestInterestingFilesInZip, self).setUp()
        self.zipfilename = pjoin(self.dir, 'foo.zip')
        with ZipFile(self.zipfilename, 'w') as z:
            z.writestr('test/file.ini', 'this should be skipped')
            z.writestr('.git/file.ini', 'this should be skipped')
            z.writestr('cool/file.ini', 'interesting, eh')
            z.close()
    def testInterestingFiles(self):
        decision_list = [(re.compile(r'.*\.ini$'), 'ini file')]
        prune_dirs = ['.git', 'test']
        results = []
        def adder():
            while True:
                results.append((yield))
        target = prime(adder())
        interesting_files_in_zip(self.zipfilename, decision_list,
                                 prune_dirs, target)
        # the last thing is going to be a lambda, and so will be
        # unique every time
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0][:3],
                         ('interesting', 'ini file', 'cool/file.ini'))


if __name__ == '__main__':
    unittest.main()
