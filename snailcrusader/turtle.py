# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin
import os.path
from textpush.match import tagged_match_or_drop

def emit_turtle_prelude(base_uri):
    def coro(target):
        while True:
            message = (yield)
            if message[0] == 'new_file':
                relativepath, = message[1:]
                file_uri = urljoin(base_uri,
                                   relativepath.replace(os.path.sep, '/'))
                target.send(('turtle_prelude', '@base <{0}> .'.format(file_uri)))
    return coro

@tagged_match_or_drop('comment', r'^t\s(.*)')
def emit_turtle_comments(match, value, target):
    target.send(('turtle_content', match[0]))

def elide_empty_turtle(target):
    """Avoid emitting @base lines where no Turtle content makes use of them."""
    cached = []
    while True:
        try:
            message = (yield)
        except GeneratorExit:
            # it's over! send whatever we had from the last file
            if any(x for x in cached if x[0] == 'turtle_content'):
                for x in cached:
                    target.send(x)
            raise
        if message[0] == 'new_file':
            if any(x for x in cached if x[0] == 'turtle_content'):
                for x in cached:
                    target.send(x)
            cached = []
        elif message[0] == 'turtle_prelude' or message[0] == 'turtle_content':
            cached.append(message)
        elif message[0] == 'turtle_prefixes':
            target.send(message)
        else:
            pass
        
def turtle_to_stdout(target):
    while True:
        message = (yield)
        if message[0] in ('turtle_prefixes',
                          'turtle_prelude',
                          'turtle_content'):
            print(message[1])
