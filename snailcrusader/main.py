# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
from .inputs import (
    interesting_files_on_disk, interesting_files_in_tar,
    interesting_files_in_zip, lines_through_commentfinder)
from .turtle import (
    emit_turtle_prelude, emit_turtle_comments, elide_empty_turtle,
    turtle_to_stdout)
from .config import Config, CONFIG_LOCATIONS
from functools import partial
from getopt import getopt
from textpush import (
    pipe, annotate_after, log as log_coro, prime, null, take_tagged,
    splitmerge)
import logging

def run(interesting_files_into):
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    log = logging.getLogger('run')
    c = Config()
    decisions = c.comment_finding_rules()
    prune_dirs = c.prune_dirs()
    base_uri = 'http://example.org/'
    turtle_prefixes = ['@prefix {0}: <{1}> .'.format(prefix, uri)
                       for prefix, uri in c.items('namespaces')]
    p = prime(pipe(
        annotate_after(lines_through_commentfinder()),
        annotate_after(emit_turtle_prelude(base_uri)),
        annotate_after(emit_turtle_comments),
        elide_empty_turtle,
        turtle_to_stdout,
        null,
        None))
    for line in turtle_prefixes:
        p.send(('turtle_prefixes', line))
    interesting_files_into(decisions, prune_dirs, p)

def usage():
    print >> sys.stderr, """\
Usage: %(progname)s <DIRECTORY|ARCHIVEFILE>

where DIRECTORY is the name of a directory containing input files, or
ARCHIVEFILE is the name of a zip, tar.gz or tar.bz2 file containing
input files.

""" % {'progname': sys.argv[0]}
    sys.exit(1)

if __name__ == '__main__':
    filesdirs = sys.argv[1:]
    if len(filesdirs) == 0:
        usage()
    if any(x == '--help' or x == '-h' for x in filesdirs):
        usage()
    # well, i wasn't planning on accepting multiple arguments but it's
    # easier to
    for fn in filesdirs:
        if os.path.isdir(fn):
            interesting_files_into = partial(interesting_files_on_disk, fn)
        elif fn.endswith('.zip'):
            interesting_files_into = partial(
                interesting_files_in_zip, fn)
        elif any(fn.endswith(x) for x in ('.tar', '.tar.gz', '.tar.bz2')):
                interesting_files_into = partial(
                    interesting_files_in_tar, fn)
        run(interesting_files_into)

