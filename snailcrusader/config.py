# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ConfigParser import SafeConfigParser
from pkg_resources import resource_stream
from os import getenv
from os.path import join
import re
import logging

CONFIG_LOCATIONS = (
    # http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
    join(getenv('HOME'), '.config', 'snailcrusader', 'config.ini'),
    join(getenv('HOME'), '.snailcrusader', 'config.ini'),
    # project-local
    join('.snailcrusader', 'config.ini'),
)

class Config(SafeConfigParser):
    def __init__(self):
        SafeConfigParser.__init__(self)
        defaults = resource_stream('snailcrusader', 'defaults.ini')
        self.readfp(defaults)
        self.read(CONFIG_LOCATIONS)

    def comment_finding_rules(self):
        log = logging.getLogger('Config.comment_finding_rules')
        all_rules = []
        for profile in self.get('find_comments', 'rules').split():
            section = 'find_comments:' + profile
            rules = []
            for ruleindex, regex_arrow_findername in self.items(section):
                try:
                    regex, findername = regex_arrow_findername.split('=>')
                except ValueError:
                    log.error('no "=>" in the setting [%s] %s = %s ; ignoring',
                              section, ruleindex, regex_arrow_findername)
                    continue
                compiled = re.compile(regex.strip())
                rules.append((ruleindex, compiled, findername.strip()))
            # remove numbers, we only need them for ordering
            rules = [x[1:] for x in sorted(rules, key=lambda x: x[0])]
            all_rules.extend(rules)
        return all_rules

    def prune_dirs(self):
        return [x.strip() for x in
                self.get('prune', 'dirs', vars={'dirs': ''}).split('\n')]
