# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from re import compile as rec
from snailcrusader import commentfinders
from functools import partial
from contextlib import contextmanager
from textpush import prime, annotate_after, identity
from zipfile import ZipFile
import tarfile
import logging

def interesting_files_on_disk(topdir, decision_list, prune_dirs, target):
    """Use the decision list to find and yield interesting files.

    decision_list should be a list [(regex, name), ...]. For each file
    anywhere under each directory in dirs (but not looking in any of
    prune_dirs), if a regex from the list matches the full pathname,
    target is sent the tuple ('interesting', the name from the list,
    the pathname relative to the directory we were looking in, and a
    context manager which takes no parameters and yields a file-like
    object).

    """
    def make_context(fullpath, relativepath):
        @contextmanager
        def context():
            with open(fullpath, 'rt') as f:
                yield f
        return context
    def prune(dirs):
        for tp in prune_dirs:
            if tp in dirs:
                dirs.remove(tp)
    for here, dirnames, filenames in os.walk(topdir):
        prune(dirnames)
        for fn in filenames:
            full = os.path.join(here, fn)
            relative = os.path.relpath(full, topdir)
            for regex, findername in decision_list:
                if regex.match(relative):
                    target.send(('interesting', findername, relative,
                                 make_context(full, relative)))
                    # we will only use one comment finder
                    break

def interesting_files_in_archive(archive_class, list_method,
                                 get_filename_callable,
                                 get_flo_context, filename,
                                 decision_list, prune_dirs, target):
    """Find and yield interesting files inside an archive.

    archive_class should be a class that opens an archive stored in a
    file. list_method should be a method of that class that gets
    information objects about members of an archive - for example,
    ZipFile.infolist. get_filename_callable should be a callable that
    takes an info object and returns the name of the file inside the
    archive, e.g. lambda info: info.filename. get_flo_context should
    be a context manager that takes the archive object and an info
    object as parameters and yields a file-like object.

    decision_list should be a list [(regex, name), ...]. For each file
    whose pathname inside the archive does not contain an element
    equal to any of the prune_dirs, if a regex from the list matches
    the full pathname, target is sent the tuple ('interesting', the
    name from the list, the pathname inside the archive, and a context
    manager which takes no parameters and yields a file-like object).

    """
    z = archive_class(filename)
    for info in list_method(z):
        name = get_filename_callable(info)
        # Unlike the os.walk above, we can't skip a whole directory,
        # because directories aren't places of their own in a zip
        # file, just parts of the names of members.
        elements = name.split(os.path.sep)
        if any(tp in elements[:-1] for tp in prune_dirs):
            continue
        for regex, findername in decision_list:
            if regex.match(name):
                target.send(('interesting', findername, name,
                             lambda: get_flo_context(z, info)))
                # we will only use one comment finder
                break

@contextmanager
def file_in_zip(archive, info):
    yield archive.open(info)

interesting_files_in_zip = partial(interesting_files_in_archive,
                                   ZipFile, ZipFile.infolist,
                                   lambda info: info.filename,
                                   file_in_zip)

@contextmanager
def file_in_tar(archive, info):
    yield archive.extractfile(info)

interesting_files_in_tar = partial(interesting_files_in_archive,
                                   tarfile.open, tarfile.TarFile.getmembers,
                                   lambda info: info.name,
                                   file_in_tar)

@contextmanager
def file_context_manager(fullpath, relativepath):
    with open(fullpath, 'rt') as f:
        yield f

class NoSuchCommentFinder(Exception):
    pass

def lines_through_commentfinder():
    def coro(target):
        finders = {}
        while True:
            message = (yield)
            if message[0] == 'interesting':
                findername, relativepath, get_content = message[1:]
                if findername not in finders:
                    try:
                        cmfdr = getattr(commentfinders, findername)
                    except AttributeError:
                        raise NoSuchCommentFinder(findername,
                                                  'while trying to process',
                                                  relativepath)
                    finders[findername] = prime(annotate_after(cmfdr)(target))
                finders[findername].send(('new_file', relativepath))
                with get_content() as f:
                    lineno = 1
                    for line in f:
                        finders[findername].send(('line', line.strip(), lineno))
                        lineno += 1
                finders[findername].send(('end_of_file', relativepath))
    return coro
