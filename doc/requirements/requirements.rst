Setting
-------

Everyone needs more computer and network security. Cyber offensive
capabilities and cyber defensive capabilities are out of
balance. Those who spend time and money on security (i.e., cyber
defense) can attain it. But it should be easier and cheaper to get
more security.

Documents exist that are publicly available and convey how to
configure some operating systems and applications for greater
security. Some nice ones are written in XCCDF, a well-regimented
format that can provide data to produce a document for people to see,
or provide data to programs to digest, equally well.

Many people could gain benefit by following some of the security rules
in these documents, whether the rules are binding on them or not.


Audience
--------

Quentin runs a Debian GNU/Linux server at home. Every time he
reinstalls it, he forgets to save the configuration files and the list
of packages he installed.  He's started using Salt to keep track of
the configuration. He wants to make his Debian box secure, but doesn't
have a lot of time to spend on it.

John administers Red Hat Enterprise Linux servers for Largeco, using
Puppet.  He's got compliance concerns to deal with, but he still needs
to bend the rules in a couple of places. Of course, he has to keep
track of those, justify them, and fix them whenever it becomes
possible.

James is John's brother, working at Otherbigco. Same concerns,
different compliance posture.

Valerie keeps up the Debian security guide content. She watches for
new requirements documents to come out, and updates the security guide
to match.

Lars maintains a Puppet module that configures the SSH server. He
wants to enable compliance with various institutional requirements,
but he wants other people to submit patches to do it.

Fatimah works in QA at Largeco. She wants to make sure John is
compliant. She doesn't deal in details of system configuration, only
details of compliance.


How to comply
-------------

There are several ways to approach the problem of compliance with
security rules.

A. Configure each system manually, following a requiring document each
time.  Quentin doesn't have time to do this. John hates it because it
repeats a lot of work, particularly in areas where exceptions need to
be made, and the assurances of compliance he writes quickly fall out
of date. Fatimah hates this because she can't easily be sure John did
his job. Lars and Valerie are not involved.

B. Automate configuration in-house; write a separate document about
compliance.  Quentin doesn't have time to do this. John can do it, but
the configuration falls out of sync with the document and he has to
spend a week every year syncing them up. Fatimah secretly wishes John
would keep things up better, but he's busy on other things. Lars would
have a cooler SSH module if John could share his code. Valerie could
ensure better compliance if John could share his code.

C. Automate configuration; use external modules where possible. Lars
can help John if someone has given Lars the code to do compliance; but
John still needs to prove to Fatimah that he's complying with
requirements when he uses Lars' code. (Lars needs technical
documentation for his users, but Fatimah isn't interested in technical
details.) Whether John gives Lars the code or someone else does,
assurances of compliance need to come with the patches. If these are
good enough, and Valerie does her job, anyone can test the claims of
compliance.


Requirements
------------

Not surprisingly, option C is the best, but it requires a way to
convey claims of compliance, and statements about those claims, having
several attributes:

The claims and statements must be written in a machine-processable
format, because there are (e.g.) thousands of modules for Puppet, and
thousands of security rules. It's the wrong scale for humans.

The claims and statements must be unobtrusive, so Lars is not bogged
down by the addition of the claims. This means (in part) that they
need to be in some text-based format, so they can be
version-controlled along with the rest of the code.

The claims and statements must have global scope, so that they don't
all need to be stated in the same place: Lars won't be interested in
all possible statements John may need to make, and John may not be
comfortable making all those statements in public.

The claims and statements should be made in a way that it becomes
clear how to obtain the compliance they claim. Exhaustive clarity in
every case, so that a script could achieve the goal, may require as
much or more complexity in the claims of compliance as resides the
language in which the code is written (e.g. "such-and-such variable
must be set greater than bla and less than bleu"), so this may not be
practical to implement.

There should be some way to associate some prose with a claim, with a
statement about that claim, or possibly with the code that the claim
is about. The prose may be intended for any of various audiences, for
example, John, James, Lars, or Fatimah.
