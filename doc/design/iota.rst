-----------
Design Iota
-----------

Goal
----

Express compliance with security rules right beside the code that
implements that compliance, so that maintenance of the code may easily
result in maintenance of the comments.

Out of scope for this design: a way to express and find results of
tests of the compliance claims.


Design
------

In the top level directory of a Puppet module, a file
``.snailcrusader.iota.ttl`` is placed. The file looks like so::

  # This file is used in expression of security rules with which this
  # module complies. See <https://gitlab.com/sagemincer/snailcrusader>
  # for more details.

  @prefix secreq: <http://securityrules.info/secreq/1#> .
  @prefix cybercompliance: <http://securityrules.info/cybercompliance/1#> .
  @prefix iota: <http://securityrules.info/snailcrusader_iota/1#> .
  @prefix ad2003stig: <http://securityrules.info/id/xiger-harap-kapop-vesix/> .
  @prefix prose: <http://example.com/prose/1#> .
  
  @base     <https://forge.puppetlabs.com/jaredjennings/mac_plist_value> .
  @prefix : <https://forge.puppetlabs.com/jaredjennings/mac_plist_value> .

  <> iota:filesFoundAt <https://githosting.example.com/jaredjennings/puppet-mac_plist_value> .

In other files in the Puppet module, comments like this are
written. Let us say this is in the ``manifests/init.pp`` file--::

  #t ad2003stig:V-4408 secreq:ramifiesTo <#tolptrue> .
  #t <#tolptrue> a secreq:Requirement ;
  #t    rdfs:description """You must give 'true' as the value of
  #t turn_off_logins_p.""" .
  define mac_plist_value($fnord, $blart, $turn_off_logins_p=false) {
  
  #t <#turn_off_logins> cybercompliance:implements ad2003stig:V-4408 ;
  #t    prose:imperativeExplainer """
  #t Turn off logins at night.
  #t """^^prose:reST .
      if $turn_off_logins_p == true {
	  class { 'mac_plist_value::logins':
	      disable_beginning => "1800",
	      disable_ending => "0600",
          }
      }
  # ...
  }

(The requirement cited and the behavior coded are utter nonsense; the
syntax is what counts.)

For each file of interest, read ``.snailcrusader.iota.ttl``, then set
the @base to the value of the ``iota:filesFoundAt`` property plus the
relative path of the file. Then read the contents of all the ``#t``
comments in the file. Take the assemblage to be a Turtle file; parse
or store as necessary.

The result is the same set of facts that Design Zeta can produce, but
they were written in many places, and their subjects are identifiers
inside files, not the module as a whole. There is a risk that other
tools may mis-parse the ``#t`` comments; and editors won't know how to
word-wrap them properly without added support. The identifiers are
duplicative with statements made in a decent language; but in, say, an
``audit.rules`` file, human-created identifiers would be all you have
to go on.

