Designs
-------

The commenting dynamic
......................

Nearly every language used for code and configuration has the idea of
a "comment" in it. Comments usually can be interspersed with code. The
proximity of a comment to a piece of code is a powerful clue to
readers that the comment is about the code, and could make it easier
for those modifying the code to keep the comment up to date. And while
the tool reading the file ignores all the comments, other tools may
pay attention to them. (In fact, some tools do pay attention to some
comments: for example, there are comments indicating the character
encoding used in the file, hashbangs at the beginning to tell the OS
what interpreter to use, comments interpreted by editors to set tab
stops and things...) If comments are to be separated from the code in
a build output, the comments must be carefully constructed in view of
this lack of context, and there must be a way to reference pertinent
code from detached comments and vice versa.


Put claims in comments
......................

α. Comments written in the code assert compliance using a human
language. This means the claims are not machine-processable, and the
prose may be limited in scope: it may be useful for John but not
James.

β. A markup language is assumed to be used in the comments; the markup
language is extended to encode compliance claims. Compliance can be
asserted tersely; useful typographical features may be pulled into
play to notate and index compliance claims. Prose regarding the claims
can be included, and possibly separated for different audiences. But
if the markup language chosen for the comments is not already used by
all possible modules of interest, patches implementing and documenting
compliance would not be accepted into upstream projects. And all the
possible prose has to be in one place.

γ. A markup language is assumed to be used in the comments, and
assumed to itself support comments. Compliance claims are written in
meta-comments. Since the markup language is not being used to mark up
the compliance claims, a tool that understands the claims must bear
the entire burden of making them visible in the markup
language. Pieces of uncommented text may be programmatically
associated with compliance claims, but the same exactitude problems as
β apply.  Prose can be associated with claims and separated by the
necessary tool.

ι. An RDF syntax is assumed to be used in the comments. Compliance
claims are written as RDF triples; prose is written as an RDF triple
with a lengthy string value. Any markup language can be used for any
comment. When any RDF triple is written near a piece of code, the
subject of the triple is nearly the piece of text after the
comment. But how to make that a URI, and a stable one, is not clear
(see also `Aside`_ below); and existing RDF syntaxes compel the
subject to be explicitly expressed. As well, claims written in
something like RDF/XML would get too verbose, but claims written in
something like Turtle would require prior state, like Turtle's prefix
statements. Ideally this could be expressed once, projectwide, rather
than at the top of each file. But this idea contains more than one
project's worth of new material.


Put claims in code
..................

δ. The language of the code is extended to encode compliance
claims. Compliance can be asserted tersely; compliance claims are
bound to syntactic structures.  But each code language in which
compliance claims are embedded must be extended. This has unbounded
difficulty: for example, languages used in configuration files for
well-established applications are not easy to change. This method does
not of itself provide a way to associate prose with compliance claims,
nor to separate it by audience, or write it in multiple places.


Inside-out
..........

ε. Instead of documentation being written in comments in the code,
code is written in amongst documentation. The documentation language
is extended to convey compliance claims. Compliance claims can be
programmatically found and cataloged. Prose can be associated and
separated. But now the code is not usable without
preprocessing. That's highly obtrusive.


Aside
.....

All of these are possibly less obtrusive than putting strange comments
in the code, but less powerful, and make less use of the power
residing in the proximity of comments to code.

ζ. A file is written in the top directory of the source of a module of
code. It contains the claims of compliance. This is very simple, and
it could work if the claims don't change much, and if the claims are
weak ("this code enables compliance with X" rather than "this code,
right here, directly implements X"). It's unobtrusive, but won't
support complex claims or prose.

η. Comments contain global identifiers; claims of compliance are
written aside from the code, using the identifiers. When the code
changes significantly, the identifiers should change, and this isn't
intrinsically clear.

θ. The tool understands the language used and concocts global
identifiers from its syntax tree. Claims of compliance are written
aside from the code, using the identifiers. These identifiers would
never have to be written, but they could lack too much stability to be
any good.
