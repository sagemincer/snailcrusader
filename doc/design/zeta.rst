-----------
Design Zeta
-----------

Goal
----

Express that a Puppet module can fulfill a set of security
requirements. Enable those limited claims to be found for any Puppet
module.

Out of scope for this design: prose associated with compliance claims;
the expression of the conditions necessary (parameter values which
must be supplied, classes which must be included, or such like) for
the claims to come true; a way to express and find results of tests of
the compliance claims.


Design
------

In the top level directory of a Puppet module, a file
``.snailcrusader.zeta.ttl`` is placed. The file looks like so::

  # This file expresses security rules with which this module complies.
  # See <https://gitlab.com/sagemincer/snailcrusader> for more details.

  @prefix secreq: <http://securityrules.info/secreq/1#> .
  @prefix cybercompliance: <http://securityrules.info/cybercompliance/1#> .
  @prefix zeta: <http://securityrules.info/snailcrusaderzeta/1#> .

  @base     <https://forge.puppetlabs.com/jaredjennings/mac_plist_value> .
  @prefix : <https://forge.puppetlabs.com/jaredjennings/mac_plist_value> .
  
  <> cybercompliance:implements
       <http://securityrules.info/id/xudah-pebah-nihyg-pudex/AC-1a.1.> ,
       <http://securityrules.info/id/xudah-pebah-nihyg-pudex/AC-1b.2.> .

  <http://securityrules.info/id/xudah-pebah-nihyg-pudex/AC-1a.1.>
     secreq:ramifiesTo :fips_parameter_on .
  <http://securityrules.info/id/xudah-pebah-nihyg-pudex/AC-1b.2.>
     secreq:ramifiesTo :fips_parameter_on .

  :fips_parameter_on
     a secreq:Requirement ;
     rdfs:description "When including the snarfbladdle class, you must provide the parameter fips with the value true." .

To evaluate compliance posture locally, a script searches the
PUPPETLIB path for all modules, finds all ``.snailcrusader.zeta.ttl``
files, compiles their data, follows references to requirements, and
compiles results in a suitable format.

To make a compliance wall of superpowers (see
<https://python3wos.appspot.com>), a script uses the Puppet Forge API
to find the latest version of all the modules, fetches and unzips
them, etc, etc, compiles results in a (probably different) suitable
format.
