What's it do?
-------------

snailcrusader obtains and collates metadata from comments written in
text files and archives containing them. It searches for comments
beginning with the letter 't' (and no space before it). Such comments
are taken to contain (after the 't') facts written in the Turtle
language.


Why?
----

One possible use for such a functionality is to collect, from some
code, statements written in the comments saying that the code
implements some requirement.

For example, someone might see a requirement `SV-50399r2_rule`_, which
says, "The `rexecd` service must not be running," write some Puppet
code which makes sure the `rexecd` service is turned off, and might
write a comment above the code::

  #t <#thisbitofcode_230035> <http://securityrules.info/ns/cybersecurity/1#implements> <http://securityrules.info/about/xuvav-tukyp-panor-navex/SV-50399r2_rule> .
  service { 'rexecd': enable => false, ensure => stopped }

Others may be interested to know the fact written in the
comment. snailcrusader liberates this fact from the syntax of the file
where it is written.

.. _SV-50399r2_rule: http://securityrules.info/about/xuvav-tukyp-panor-navex/SV-50399r2_rule

For each file, snailcrusader comes up with a suitable base URI, so
that URIs written inside the file (like `<#thisbitofcode_230035>`
above) can be relative, but still end up globally unique.

There are many other possible uses for snailcrusader, because there is
no limit to the statements you can make using RDF triples.


To use snailcrusader
--------------------

If you have a directory containing a simple Puppet module, just run ::

  snailcrusader .

snailcrusader will look for ``#t`` comments in your Puppet source,
templates, erb files, and ini files.

If you have different kinds of files, you'll need to give
snailcrusader some more configuration to find them, as follows. Make a
``.snailcrusader`` directory and put a file in it called
``config.ini``. Write how comments should be found. ::

  [find_comments]
  rules: a_kind_of_file_structure another_kind

I.e., put whitespace between the values. The idea is that there are
structures of files that have names, like "Puppet module," "Django web
application," "Clojure project," "Salt state," or "LaTeX multi-file
document," each of which has some rules about where to find files, and
how comments will be written in those kinds of files. Known file
structure types at present are:

 * ``puppet_module``

To define a file structure, make a section called
``[find_comments:latex_document]``, for example. Fill it with rules
linking patterns in relative pathnames to `commentfinders`. For
example::

  [find_comments:latex_document]
  101: .*\.tex => percent

This tells snailcrusader that to find comments in a `latex_document`,
it should look for files whose names end with ``.tex`` and use the
``percent`` commentfinder to find comments in them. The 101 is just
for ordering multiple rules, so that if you have some specific files
that need to be treated a certain way, the rule matching them can fire
before a more general rule that might match, but not treat them quite
properly.

Commentfinders at present are:

 * ``hash_skip_hashbang``: Comments are written with the ``#``
   character at the beginning of a line. But if the first line starts
   with ``#!``, as it may for a shell script or Python script, it is
   ignored by snailcrusader.
 * ``hash_or_semicolon``: Comments are written with either ``#`` or
   ``;`` at the beginning of the line. These are the sort of comments
   supported by Python's ConfigParser.
 * ``percent``: ``%`` at the beginning of the line. TeX/LaTeX.
 * ``erb_comments``: Comments are written inside erb's ``<% %>`` tags
   with the ``#`` character, not necessarily at the beginning of a
   line.

So in our notional LaTeX example, the entire contents of the
``.snailcrusader/config.ini`` file would be::

  [find_comments]
  rules: latex_document

  [find_comments:latex_document]
  101: .*\.tex => percent

If the file structure you have described is more general than just the
particular set of files you are working on at the moment, your rules
should be imported into snailcrusader. Please fork snailcrusader, add
your rules to ``snailcrusader/defaults.ini``, and send a merge
request. Then those who come after you will only have to write, e.g.,
::

   [find_comments]
   rules: latex_document

And someone else will have a foundation onto which to add support for
finding comments in BibTeX (for example).

If there are Turtle namespaces you would like to use in your comments,
write them in the ``namespaces`` section of
``.snailcrusader/config.ini``, for example::

  [namespaces]
  foaf: http://xmlns.com/foaf/0.1/



To work on snailcrusader
------------------------

Issues are tracked in the Gitlab tracker at
https://gitlab.com/sagemincer/snailcrusader/issues.

Please also fork and send merge requests.
