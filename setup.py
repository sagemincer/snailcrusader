# snailcrusader - gather and report security compliance from text files
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from setuptools import setup

# http://www.scotttorborg.com/python-packaging/metadata.html
def readme():
    with open('README.rst') as f:
            return f.read()

setup(
        name='snailcrusader',
        description='Gather and report security compliance from text files',
        long_description=readme(),
        version='0.1',
        author='Jared Jennings',
        author_email='jjennings@fastmail.fm',
        license='GPLv3',
        platforms='OS-independent',
        install_requires=[
            'textpush',
        ],
        dependency_links=[
            'https://gitlab.com/jaredjennings/python-textpush/repository/archive.zip#egg=textpush-1.13',
        ],
        packages=['snailcrusader'],
        package_data={'snailcrusader': ['defaults.ini']},
        )

